<?php
/**
 * @file
 * Testing page to redirect the payments.
 */

/**
 * RCCPagos testing page.
 */
function uc_rccpagos_testing_page() {
  $data = _uc_rccpagos_get_submitted_data($_POST);
  $verif = isset($_POST['rccpagosVerificacion']) ? $_POST['rccpagosVerificacion'] : FALSE;
  $checksum = _uc_rccpagos_checksum($data);

  if ($verif !== FALSE && $verif == $checksum) {
    $links = _uc_rccpagos_get_testing_links($data, $checksum);

    $output = theme_item_list(array(
      'items' => $links,
      'title' => t('Simulate RCCPagos results'),
      'type' => 'ul',
      'attributes' => array(),
    ));
  }
  else {
    $output = t('Invalid checksum');
  }

  return $output;
}

function _uc_rccpagos_get_submitted_data($raw_input) {
  $keys = array(
    'rccpagosEmpresa',
    'rccpagosImporte',
    'rccpagosMoneda',
    'rccpagosOrden',
    'rccpagosURLError',
    'rccpagosURLOk',
    'rccpagosUsuario',
    'rccpagosMedioDePago',
    'rccpagosVencimiento',
    'rccpagosCorreoElectronico',
    'rccpagosCuotas',
    'rccpagosPromocion',
  );

  foreach ($keys as $key) {
    if (isset($raw_input[$key])) {
      $data[$key] = $raw_input[$key];
    }
  }

  return $data;
}

/**
 * Returns a list of links to simulate the possible results of a payment in
 * RCCPagos.
 */
function _uc_rccpagos_get_testing_links($data, $checksum) {
  module_load_include('inc', 'uc_rccpagos', 'uc_rccpagos.pages');
  $verif = strtolower(md5($checksum . variable_get('uc_rccpagos_secret_key', '')));

  $url_ok = urldecode($data['rccpagosURLOk']);
  $url_ok = str_replace('###VERIF###', $verif, $url_ok);
  $links[] = l(t('Successful payment'), $url_ok, array('absolute' => TRUE));

  $url_failed = urldecode($data['rccpagosURLError']);
  $url_failed = str_replace('###VERIF###', $verif, $url_failed);

  // Generate responses for the common errors (see _uc_rccpagos_get_error_msg).
  foreach (array('3', '4', '5', '6', '7', '8') as $error) {
    $url = str_replace('###ERROR###', $error, $url_failed);
    $text = t('Failed payment') . ': ' . _uc_rccpagos_get_error_msg($error);
    $links[] = l($text, $url, array('absolute' => TRUE));
  }
  return $links;
}
